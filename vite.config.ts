// vite.config.ts
import UnoCSS from 'unocss/vite'
import { defineConfig } from 'vite'

export default defineConfig({
    base: '/01-huddle-landing-page',
    plugins: [
        UnoCSS(),
    ],
})
